Device Checklist
================

Working
-------
* Actors: Manual brightness
* Actors: Torchlight
* Actors: Vibration
* Bluetooth: Driver loaded at startup
* Bluetooth: Enable/disable and flightmode works
* Bluetooth: Persistent MAC address between reboots
* Bluetooth: Pairing with headset works, volume control ok
* Camera: Flashlight
* Camera: Photo
* Camera: Switch between back and front camera
* Cellular: Carrier info, signal strength
* Cellular: Data connection
* Cellular: Incoming, outgoing calls
* Cellular: Voice in calls
* Cellular: SMS in, out
* Cellular: Change audio routings
* Cellular: Enable/disable mobile data and flightmode works
* Endurance: Battery lifetime > 24h from 100%
* GPU: Boot into UI
* GPU: Hardware video decoding
* Misc: Anbox patches applied to kernel
* Misc: AppArmor patches applied to kernel
* Misc: Battery percentage
* Misc: Online charging (Green charging symbol, percentage increase in stats etc)
* Misc: Reset to factory defaults
* Misc: Recovery image builds and works
* Misc: Wireless charging - only for devices that support it
* WiFi: Driver loaded at startup
* WiFi: Enable/disable and flightmode works
* WiFi: Persistent MAC address between reboots
* WiFi: Hotspot can be configured, switched on and off, can serve data to clients
* Sensors: Fingerprint reader, register and use fingerprints (Halium 9.0 only)
* Sensors: Rotation
* Sensors: Touchscreen
* Sound: Earphones detected, volume control
* Sound: Loudspeaker
* Sound: Microphone
* Sound: Loudspeaker volume control
* USB: MTP access
* USB: ADB access

Working with additional steps
-----------------------------

Not working
-----------
* Actors: Notification LED
* Camera: Video
* Misc: Offline charging (Power down, connect USB cable, device should not boot to UT)
* Misc: Shutdown / Reboot
* Sensors: Automatic brightness
* Sensors: Proximity

Untested
--------
* Cellular: MMS in, out
* Cellular: PIN unlock
* Cellular: Switch connection speed between 2G/3G/4G works for all SIMs
* Cellular: Switch preferred SIM for calling and SMS - only for devices that support it
* Endurance: No reboot needed for 1 week
* Misc: Date and time are correct after reboot (go to flight mode before)
* Misc: SD card detection and access - only for devices that support it
* Network: NFC - only for devices that support it
* Sensors: GPS
